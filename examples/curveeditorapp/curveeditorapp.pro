TARGET = curveeditorapp

TEMPLATE = app

QT += widgets

CONFIG += debug

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += .

RESOURCES = $$PWD/../../resources/curveeditor.qrc

include($$PWD/../../sharedcomponents.pri)

# Input
HEADERS += \
    mainwindow.h \
    examplecurvemodel.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    examplecurvemodel.cpp
